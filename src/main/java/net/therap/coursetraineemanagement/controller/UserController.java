package net.therap.coursetraineemanagement.controller;

import net.therap.coursetraineemanagement.domain.Admin;
import net.therap.coursetraineemanagement.domain.Course;
import net.therap.coursetraineemanagement.domain.Trainee;
import net.therap.coursetraineemanagement.domain.User;
import net.therap.coursetraineemanagement.helper.HashGenerationHelper;
import net.therap.coursetraineemanagement.service.AdminService;
import net.therap.coursetraineemanagement.service.EnrollmentService;
import net.therap.coursetraineemanagement.service.TraineeService;
import net.therap.coursetraineemanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * @author soumik.sarker
 * @since 9/28/21
 */
public class UserController {
}
